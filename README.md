# Kube

Docker image for CI with :
* [Kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
* [Helm](https://helm.sh/)
* [Helmfile](https://github.com/roboll/helmfile)

## Build and run

```
make run
```
