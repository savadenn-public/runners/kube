FROM alpine:3

RUN apk update && \
    apk add --no-cache \
    ca-certificates \
    bash \
    curl \
    git \
    && rm -f /tmp/* /etc/apk/cache/*

ENV LANG en_US.UTF-8 
ENV HELM_VERSION="v3.6.3"
ENV HELMFILES_VERSION="v0.140.0"

RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm \
    && curl -LO "https://github.com/roboll/helmfile/releases/download/${HELMFILES_VERSION}/helmfile_linux_amd64" \
    && chmod +x ./helmfile_linux_amd64 \
    && mv ./helmfile_linux_amd64 /usr/local/bin/helmfile

RUN helm plugin install https://github.com/databus23/helm-diff \
  && helm plugin install https://github.com/aslafy-z/helm-git.git --version 0.10.0


